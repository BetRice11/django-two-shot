from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number"
    ]

@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date"
    ]

@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = [
        "name",
        "id"
    ]
# Register your models here.
